#!/bin/bash
if test -d $HOME/.config/i3; 
then
     	mv $HOME/.config/i3 $HOME/.config/i3.bak 
fi
cp -R .config/i3 $HOME/.config/i3
