#!/bin/bash



# Variables

path=$(pwd)
programs=(ifplugd vim iw i3-wm flameshot rxvt-unicode i3-gaps i3blocks dmenu ttf-iosevka-nerd nitrogen lxappearance acpi git lightdm lightdm-slick-greeter pacman-contrib alsa-utils pulseaudio light netctl dialog wpa_supplicant lightdm-gtk-greeter-settings)
aur=(i3blocks-contrib)
aur_install=()
installing=()

# Dependencies

if ! which sudo > /dev/null; then
	echo "Sorry, you need to have sudo in order to use this script. Install sudo and configure the visudo file"
	exit 1
fi

for i in "${programs[@]}";
do 
#	which $i > /dev/null || installing+=" "$i
	pacman -Q $i > /dev/null || installing+=" "$i
done
echo $installing
sudo pacman -Sy $installing


if ! which yay > /dev/null; then
	echo "Yay not found. Installing..."
	git clone https://aur.archlinux.org/yay.git
	cd yay
	makepkg -si
fi

for i in "${aur[@]}";
do 
	pacman -Q $i || aur_install+=" "$i
done
echo $aur_install
yay -Sy $aur_install

# Directory structure

while true
do
	if [ -d "$HOME/.config" ]; then
		if [ -d $HOME/.config/i3 ]; then
			if [ -e $HOME/.config/i3/config ] && [ -e $HOME/.config/i3/i3blocks.conf ]; then
				echo "You have configs for both i3 and i3blocks, remove them in order to install (a backup is reccommended)"
				exit 1
			elif [ -e $HOME/.config/i3/config ]; then
				echo "You have a config for i3, so only the i3blocks config was installed. Remove the i3 config in order to install (a backup is reccommended)"
				cp $path/.config/i3/i3blocks.conf $HOME/.config/i3/i3blocks.conf
				break
			elif [ -e $HOME/.config/i3/i3blocks.conf ]; then
				echo "You have a config for i3blocks, so only the i3 config was installed. Remove the i3blocks config in order to install (a backup is reccommended)"
				cp $path/.config/i3/config $HOME/.config/i3/config
				break
			fi
		else
			mkdir $HOME/.config/i3
			cp $path/.config/i3/config $HOME/.config/i3/config
			cp $path/.config/i3/i3blocks.conf $HOME/.config/i3/i3blocks.conf
			echo "i3 directory has been created in .config and config + i3blocks.conf files have been copied"
			break
		fi	
	else
		mkdir $HOME/.config
		mkdir $HOME/.config/i3
		cp $path/.config/i3/config $HOME/.config/i3/config
		cp $path/.config/i3/i3blocks.conf $HOME/.config/i3/i3blocks.conf
		echo ".config/ and .config/i3/ directories have been created and config + i3blocks.conf files have been copied"
	fi
done
echo "Depending on the message you got files/directories were created/copied; if you have configs somewhere else (e.g. .i3/config) then backup and edit the new config to your needs."

# Lightdm configuration

sudo systemctl enable lightdm
sudo systemctl start lightdm

if [ -d /etc/lightdm ]; then
	if [ -e /etc/lightdm/lightdm.conf ]; then
		read -r -p "Do you want to set slick-greeter as your lightdm greeter? [y/N] " response
		case "$response" in
			[yY])
				sudo sed -i 's/^#greeter-session=.*/greeter-session=lightdm-slick-greeter/' /etc/lightdm/lightdm.conf
				echo "Slick greeter successfully set as the greeter in /etc/lightdm/lightdm.conf"
				;;
			[nN])
				continue
				;;
		esac
	else
		sudo cp $path/.config/lightdm/lightdm.conf /etc/lightdm/lightdm.conf
	fi
else
	sudo mkdir /etc/lightdm/
	sudo cp $path/.config/lightdm/lightdm.conf /etc/lightdm/lightdm.conf
fi

